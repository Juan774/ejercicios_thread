package app;

public class Ejercicio3 extends Thread {
    String nombre;
    public Ejercicio3 (int prioridad,String nombre){
    this.nombre=nombre;
    setPriority(prioridad);
    
    
   }

   
 public void run(){
    for(int c=1;c<=50;c++){
    System.out.print(c+"mt ");
    yield(); 
    }
    System.out.println("\n Llego a la meta " + nombre);
   }
    static Ejercicio3 comp1;
    static Ejercicio3 comp2;
    static Ejercicio3 comp3;
    public static void main(String []args) throws InterruptedException {
    comp2 = new Ejercicio3(1," Alejandro ");
    comp1 = new Ejercicio3(5," William ");
    comp3 = new Ejercicio3(8," Juan");
    comp2.start();
    comp1.start();
    comp3.start();
    comp2.join();
    comp1.join();
    comp3.join();
    }
   }
   